const tasks = [
    { title: 'Faire les courses', isComplete: false },
    { title: 'Nettoyer la maison', isComplete: true },
    { title: 'Planter le jardin', isComplete: false }
];

const newTask = {
    title: "Faire le repas",
    isComplete: false
}

// fonction fléchée pour ajouter une tâche, clône le tableau existant
const addTask = (taskList, taskToAdd) = [...taskList, taskToAdd];


console.log("clone", addTask(tasks, newTask));

//fonction fléchée suppresion des tâches complétées
const removeCompletedTask = (taskList) => {
    const taskFiltered = taskList.filter(
        (eachTask) => {
            //valeur vrai : je garde l'élèment
            if (eachTask.isComplete === true) {
                return false;
            } else {
                // valeur fausse : je retire l'élèment 
                return true;
            }

        }
    );
    return taskFiltered;
}
// const filterCompleted = (taskList) => taskList.filter((task) => !task.isComplete)

console.log("filter", removeCompletedTask(tasks))

const toggleTask = {
    title: "Acheter le pain",
    isComplete: false
};
//fonction fléchée basculer l'état d'une tâche
const toggleTaskStatus = (task) => {
    if (task.isComplete === true) {
        task.isComplete = false;
    } else {
        task.isComplete = true
    } return task;
}

/*  const toggleTaskStatus = (task) => { 
      const clone = {...task,
           isComplete : !clone.isComplete  
      }
     return clone;
}*/

console.log("toggle", toggleTaskStatus(toggleTask))

//filtre les tâches en fonction d'un état 
const filterTaskByState = (taskList,status) => {

    /*status : "completed", "no completed", "all"*/

  return  taskList.filter((eachTask) => {
        if (status === "no completed") {
            if (eachTask.isComplete === true) {
                return false;
            } else {
                return true;
            }

        } else if (status === "completed") {
            if (eachTask.isComplete === true) {
                return true; 
            } else {
                return false;
            }
        } else {
            return true;
        }

    })
}
/*
Simplification du code 
const filterTaskByState = ((taskList, status) => {
    return taskList.filter(eachTask) => {
        if ( status === "no completed"){
            return !eachTask.isComplete
        }else if ( status === "completed"){
            return eachTask.isComplete
        } else {
            return true;
        }
    })
} */
console.log("filterStatus", filterTaskByState(tasks, "all"));

